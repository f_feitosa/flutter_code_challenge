# 🧮 Calculify - Fluter Coding Challenge

## The Challenge

The challenge consists in implementing a calculator in Flutter.

### Requisites:

● Build simple user interface

● Global app state & local app state passed between views

● Implementation of navigation flows

● Error handling with user feedback

The calculator to build should be able to perform multiple simple calculations. The supported operations must be: Addition, Subtraction, Multiply, Divide, and Square root.

The first screen is where the user is supposed to pick an operation. They should be presented with a list of the supported operations.

The second screen is where users can perform calculations using the operator selected in the screen before. The built-in number input keyboard can be used. A custom digit input widget can be implemented, but is not required.
A cancel button should allow the user to go back to operation selection and stash all already entered data. The same screen should be used twice, in case the operation is not square root, as stated on the diagram bellow.

![Screen Shot 2021-11-17 at 10 34 03](https://user-images.githubusercontent.com/3979786/142174928-275f0dc4-151f-4ea9-8cef-74f9f3a52f95.png)

In the last view, the result of the calculation is shown. Additionally, a button should allow the restart with a new calculation.

- Null safety
Use dart new sound null safety.

- Design
  The design should be practical and pragmatic. It is not about eye candy here. A clean simple layout should be enough!

- Testing
  The app should be runnable. Also, a unit and integration suite would be highly recommended.
  
Error handling:
The user should be alerted when they try to calculate a square root value with a negative amount, and when they try to calculate a division by 0.

# Implementation

## Project structure:
<img width="387" alt="Screen Shot 2021-11-18 at 00 40 22" src="https://user-images.githubusercontent.com/3979786/142299803-02c38819-fb63-4fd5-8a35-b37b5d5a2eab.png">


I decided to structure the files by features. This makes things easier in case I want to completely remove a feature. (even though in this example we only have one view on two of the "features", I assumed it could get more complex if more logic was introduced.

## State Management:

For state management, I decided to use *Bloc* (with fluter_bloc and equatable libraries). In that way, I manage to control the application state using StatelessWidgets (no `setState` ✨) and properly separate business logic from the UI. The view is passive and only reacts to the state changes.

![Screen Shot 2021-11-17 at 10 33 20](https://user-images.githubusercontent.com/3979786/142174835-09b2a7f7-4810-4fdf-ad63-d6dac939718a.png)

## Error handling:

For the error handling, I decided to create *Custom Exceptions* with a body specifying why there was an error. By using exceptions to handle the edge cases, I can wrap the calculationValidation and inputValidation methods in a try catch, if there's one error, I just need to emit a new instance of my state with the error message so my view can show a Snackbar.
All custom exceptions implements `CalculatorException`, which corresponds to errors in the calculator flow. If we introduced new features, I would create another custom exception for this new feature. I couldn't find much consensus on how to handle errors using exceptions, and one article I found generated a lot of boilerplate, so I decided t emit a state with an error and right afterwards emiting another state with the previous state value, which wouldn't cause any change in the screen and would avoid any error to be shown afterwards.

The challenge specified that I should prevent the calculation and give users a feedback message when they tried to:
- Divide by 0 (`CantDivideByZeroException`)
- Calculate the square root of a negative value (`CantSquareRootNegativeNumberException`).

Other than the scenarios specified in the challenge, I also covered the following scenarios:

- Trying to input a `.` with no numbers `NeedsNumberBeforeDecimalException`;
- Trying to calculate values with no number `NeedsAtLeastOneNumberException`;
- Trying to add more than one Decimal `OnlyOneDecimalAllowedException`;
- Trying to calculate operations that are not square root with only one number, `OperationNeedsTwoNumbers`


![snack_error](https://user-images.githubusercontent.com/3979786/142379762-dadef494-31a6-46db-b480-eeac8b693a1a.gif)

## Tests

For the challenge, I created Unit tests for the calculator logic, reactive Bloc tests to make sure all `success events` and `errors` are handled properly, and a few UI tests to make sure the app display all the information necessary, I reckon the UI tests in this cases aren't testing much and could be left out for the test, but I was having fun learning how to create UI tests in Flutter!

### Runnning the tests:
`flutter test`

![Screen Shot 2021-11-20 at 18 10 40](https://user-images.githubusercontent.com/3979786/142735698-69e0e591-3c85-448c-9e8c-99770c7388db.png)

### Known issues

There's a small bug on the custom TextField I implemented where when adding a decimal, the `.` will show at the start of the TextField instead of on end, but this is corrected after typing one number.
Considering I didn't have time in the world to create this app, I decided not to dig deeper on how to fix this issue so I could focus on other aspects of the challenge.

### Considerations
I had a lot of fun doing this challenge. As I do not work full-time with Flutter, I ended up learning some concepts I didn't know before during the implementation. :)

Thanks for reading! 👋

![calculation_flow](https://user-images.githubusercontent.com/3979786/142380051-98a0cb53-9710-40d5-ae11-858632654b7c.gif)



