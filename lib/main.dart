import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/select_operator/select_operator_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Challenge',
      initialRoute: '/',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.grey[900],
        primarySwatch: Colors.deepOrange,
      ),
      home: const SelectOperatorScreen(),
    );
  }
}
