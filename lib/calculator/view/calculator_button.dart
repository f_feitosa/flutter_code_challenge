import 'package:flutter/material.dart';

class CalculatorButton extends StatelessWidget {
  final String buttonValue;
  final VoidCallback onPressed;

  const CalculatorButton(
      {Key? key, required this.buttonValue, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ButtonTheme(
        child: ElevatedButton(
          child: Text(
            buttonValue,
            style: const TextStyle(
                fontSize: 24, color: Colors.white, fontWeight: FontWeight.w700),
          ),
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
            fixedSize: const Size(10.0, 10.0),
            primary:
                buttonValue == "-" ? Colors.deepOrangeAccent : Colors.blueGrey,
            shape: const CircleBorder(),
          ),
        ),
      ),
    );
  }
}
