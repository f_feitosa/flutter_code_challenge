import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'package:flutter_code_challenge/shared/components/custom_outlined_button.dart';
import 'package:flutter_code_challenge/shared/nav.dart';

import '../bloc/calculator_bloc.dart';
import 'calculator_button.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'calculator_button.dart';

class CalculatorScreen extends StatelessWidget {
  final CalculatorOperation operation;
  final String? previousNumber;

  const CalculatorScreen(
      {Key? key, required this.operation, this.previousNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CalculatorBloc>(
      create: (_) =>
          CalculatorBloc(operation: operation, previousNumber: previousNumber),
      child: SafeArea(
        child: Scaffold(
            body: Column(
          children: [
            const InputArea(),
            CalculatorButtons(),
            const ActionButtons()
          ],
        )),
      ),
    );
  }
}

class InputArea extends StatelessWidget {
  const InputArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController();

    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        decoration: const BoxDecoration(
            color: Colors.blueGrey,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40.0),
                bottomRight: Radius.circular(40.0))),
        child: BlocConsumer<CalculatorBloc, CalculatorState>(
          listener: (context, state) {
            switch (state.runtimeType) {
              case InputNextNumber:
                {
                  final nextNumberState = state as InputNextNumber;
                  Nav.toCalculator(
                      context: context,
                      operation: nextNumberState.operation,
                      previousNumber: nextNumberState.previousNumber);
                  break;
                }
              case CalculatorView:
                {
                  final viewState = state as CalculatorView;
                  _controller.text = state.currentNumberForRTLInputText ?? "";

                  if (viewState.error != null) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text(viewState.error!)));
                  }
                  break;
                }
              case CalculationResult:
                {
                  Nav.toResult(
                      context: context,
                      calculationResult: (state as CalculationResult).result);
                  break;
                }
            }
          },
          builder: (context, state) {
            return Column(
              children: [
                Text(
                  state is CalculatorView
                      ? state.previousNumberWithOperator
                      : "",
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
                TextField(
                  controller: _controller,
                  decoration: const InputDecoration(border: InputBorder.none),
                  readOnly: true,
                  showCursor: true,
                  cursorWidth: 3.0,
                  autofocus: true,
                  cursorColor: Colors.deepOrangeAccent,
                  textDirection: TextDirection.rtl,
                  style: const TextStyle(
                      fontSize: 30.0, height: 2.0, color: Colors.white),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class CalculatorButtons extends StatelessWidget {
  CalculatorButtons({Key? key}) : super(key: key);

  final List<String> _buttons = [
    "7",
    "8",
    "9",
    "4",
    "5",
    "6",
    "1",
    "2",
    "3",
    "0",
    ".",
    "-",
  ];

  @override
  Widget build(BuildContext context) {
    final _bloc = BlocProvider.of<CalculatorBloc>(context);

    return Expanded(
      flex: 3,
      child: Container(
        margin: const EdgeInsets.only(
            left: 30.0, top: 10.0, right: 30.0, bottom: 0.0),
        child: GridView.builder(
            itemCount: _buttons.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            itemBuilder: (_, int i) {
              return CalculatorButton(
                  buttonValue: _buttons[i],
                  onPressed: () {
                    _bloc.add(Typing(digit: _buttons[i]));
                  });
            }),
      ),
    );
  }
}

class ActionButtons extends StatelessWidget {
  const ActionButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _bloc = BlocProvider.of<CalculatorBloc>(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomOutlinedButton(
              text: "Cancel",
              callback: () {
                Nav.restart(context);
              },
              textColor: Colors.deepOrangeAccent),
          BlocBuilder<CalculatorBloc, CalculatorState>(
            builder: (context, state) {
              if (state is CalculatorView && state.isCalculateResultScreen) {
                return CustomOutlinedButton(
                    text: "Calculate",
                    callback: () {
                      _bloc.add(Calculate());
                    },
                    textColor: Colors.deepOrangeAccent);
              } else {
                return CustomOutlinedButton(
                    text: "Insert next value",
                    callback: () {
                      _bloc.add(InsertNextNumber());
                    },
                    textColor: Colors.deepOrangeAccent);
              }
            },
          )
        ],
      ),
    );
  }
}
