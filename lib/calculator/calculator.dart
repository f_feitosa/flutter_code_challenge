import 'dart:math';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'bloc/calculator_bloc.dart';
import 'calculator_exception.dart';

class Calculator {
  static String calculate(CalculatorView viewState) {
    try {
      _validateCalculation(viewState: viewState);
      return _calculateByOperator(viewState);
    } catch (e) {
      rethrow;
    }
  }

  static void _validateCalculation({required CalculatorView viewState}) {
    if (viewState.currentNumber == null ||
        viewState.currentNumber!.isEmpty == true) {
      throw NeedsAtLeastOneNumberException();
    }

    if (viewState.operation == CalculatorOperation.squareRoot &&
        viewState.currentNumber!.contains("-")) {
      throw CantSquareRootNegativeNumberException();
    }

    if (viewState.operation != CalculatorOperation.squareRoot &&
        ((viewState.currentNumber == null ||
                viewState.currentNumber?.isEmpty == true) ||
            (viewState.previousNumber == null ||
                viewState.previousNumber?.isEmpty == true))) {
      throw OperationNeedsTwoNumbers();
    }

    if (viewState.operation == CalculatorOperation.divide &&
            viewState.currentNumber == "0" ||
        viewState.previousNumber == "0") {
      throw CantDivideByZeroException();
    }
  }

  static String _calculateByOperator(CalculatorView viewState) {
    switch (viewState.operation) {
      case CalculatorOperation.addition:
        {
          return (double.parse(viewState.previousNumber!) +
                  double.parse(viewState.currentNumber!))
              .toString();
        }
      case CalculatorOperation.subtraction:
        {
          return (double.parse(viewState.previousNumber!) -
                  double.parse(viewState.currentNumber!))
              .toString();
        }
      case CalculatorOperation.multiply:
        {
          return (double.parse(viewState.previousNumber!) *
                  double.parse(viewState.currentNumber!))
              .toString();
        }
      case CalculatorOperation.divide:
        {
          return (double.parse(viewState.previousNumber!) /
                  double.parse(viewState.currentNumber!))
              .toString();
        }
      case CalculatorOperation.squareRoot:
        {
          return sqrt(double.parse(viewState.currentNumber!)).toString();
        }
    }
  }
}
