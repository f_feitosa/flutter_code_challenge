part of 'calculator_bloc.dart';

abstract class CalculatorEvent extends Equatable {
  const CalculatorEvent();
}

class Typing extends CalculatorEvent {
  final String digit;

  const Typing({required this.digit});

  @override
  List<Object> get props => [digit];
}

class Calculate extends CalculatorEvent {
  @override
  List<Object> get props => [];
}

class InsertNextNumber extends CalculatorEvent {
  @override
  List<Object> get props => [];
}
