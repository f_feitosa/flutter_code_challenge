part of 'calculator_bloc.dart';

abstract class CalculatorState extends Equatable {
  const CalculatorState();
}

class CalculatorView extends CalculatorState {
  final String? currentNumber;
  final String? previousNumber;
  final CalculatorOperation operation;
  String? error;

  bool get isCalculateResultScreen =>
      operation == CalculatorOperation.squareRoot ||
      previousNumber?.isNotEmpty == true;

  String? get currentNumberForRTLInputText =>
      currentNumber != null && currentNumber!.contains("-")
          ? "${currentNumber!.replaceAll("-", "")}-"
          : currentNumber;

  String get previousNumberWithOperator => previousNumber?.isNotEmpty == true
      ? "${previousNumber}${operation.operationValue}"
      : "";

  CalculatorView(
      {this.currentNumber,
      this.previousNumber,
      required this.operation,
      this.error});

  CalculatorView copyWith(
          {String? currentNumber, String? previousNumber, String? error}) =>
      CalculatorView(
          currentNumber: currentNumber ?? this.currentNumber,
          previousNumber: previousNumber ?? this.previousNumber,
          operation: operation,
          error: error ?? this.error);

  @override
  List<Object?> get props => [
        currentNumber,
        previousNumber,
        operation,
        error,
        isCalculateResultScreen
      ];
}

class InputNextNumber extends CalculatorState {
  final String previousNumber;
  final CalculatorOperation operation;

  const InputNextNumber(
      {required this.previousNumber, required this.operation});

  @override
  List<Object> get props => [previousNumber, operation];
}

class CalculationResult extends CalculatorState {
  final String result;

  CalculationResult({required this.result});

  @override
  List<Object?> get props => [result];
}
