import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';

import '../calculator.dart';
import '../calculator_exception.dart';

part 'calculator_event.dart';

part 'calculator_state.dart';


class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  CalculatorBloc(
      {required CalculatorOperation operation, String? previousNumber})
      : super(CalculatorView(
      operation: operation, previousNumber: previousNumber));

  @override
  Stream<CalculatorState> mapEventToState(CalculatorEvent event) async* {
    if (state is CalculatorView) {
      final viewState = state as CalculatorView;
      switch (event.runtimeType) {
        case Typing:
          {
            final Typing typingEvent = event as Typing;
            try {
              _inputValidation(viewState: viewState, event: typingEvent);
              final formattedCurrentNumber = _formattedCurrentNumber(
                  viewState: viewState, event: typingEvent);
              yield viewState.copyWith(currentNumber: formattedCurrentNumber);
            } on CalculatorException catch (e) {
              yield* _emitError(viewState, e);
            }
            break;
          }
        case Calculate:
          {
            yield* _calculate(viewState);
            break;
          }
        case InsertNextNumber:
          {
            yield InputNextNumber(
                previousNumber: viewState.currentNumber!,
                operation: viewState.operation);
            break;
          }
      }
    }
  }

  Stream<CalculatorState> _calculate(CalculatorView viewState) async* {
    if (viewState.isCalculateResultScreen) {
      try {
        var calculation = Calculator.calculate(viewState);
        yield CalculationResult(result: calculation);
      } on CalculatorException catch (e) {
        yield* _emitError(viewState, e);
      }
    }
  }

  Stream<CalculatorState> _emitError(CalculatorView viewState, CalculatorException e) async* {
    yield viewState.copyWith(error: e.body);
    yield viewState.copyWith(error: null);
  }

  void _inputValidation(
      {required CalculatorView viewState, required Typing event}) {
    if (event.digit == '.') {
      if (viewState.currentNumber == null ||
          viewState.currentNumber?.length == 1 &&
              viewState.currentNumber?.contains("-") == true) {
        throw NeedsNumberBeforeDecimalException();
      }

      if (viewState.currentNumber?.contains(".") == true) {
        throw OnlyOneDecimalAllowedException();
      }
    }
  }

  String _formattedCurrentNumber(
      {required CalculatorView viewState, required Typing event}) {
    if (viewState.currentNumber != null) {
      if (viewState.currentNumber!.contains("-")) {
        String negativeCurrentNumber =
            "-${viewState.currentNumber!.replaceAll("-", "")}";
        return "$negativeCurrentNumber${event.digit}";
      }
    }

    return "${viewState.currentNumber ?? ""}${event.digit}";
  }
}