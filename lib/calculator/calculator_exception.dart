class CalculatorException implements Exception {
  String body;

  CalculatorException({required this.body});
}

class NeedsNumberBeforeDecimalException implements CalculatorException {
  @override
  String body = "You need to add at least one number before adding a decimal.";
}

class NeedsAtLeastOneNumberException implements CalculatorException {
  @override
  String body = "You need to add a number.";
}

class OnlyOneDecimalAllowedException implements CalculatorException {
  @override
  String body = "You can only add one decimal.";
}

class CantSquareRootNegativeNumberException implements CalculatorException {
  @override
  String body =
      "You can't do this operation with negative numbers, please restart your calculation";
}

class CantDivideByZeroException implements CalculatorException {
  @override
  String body = "You can't divide by zero, please restart your calculation";
}

class OperationNeedsTwoNumbers implements CalculatorException {
  @override
  String body = "You need to input two numbers to perform this operation";
}
