import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/shared/components/custom_outlined_button.dart';
import 'package:flutter_code_challenge/shared/components/header.dart';
import 'package:flutter_code_challenge/shared/nav.dart';

class ResultScreen extends StatelessWidget {
  final String calculationResult;

  const ResultScreen({Key? key, required this.calculationResult})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Header(text: "Congratulations ✨"),
            const SizedBox(
              height: 20.0,
            ),
            const Text("your result is:",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w300)),
            Text(calculationResult,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w900)),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: SizedBox(
                width: double.infinity,
                height: 50.0,
                child: CustomOutlinedButton(
                    text: "Restart",
                    callback: () {
                      Nav.restart(context);
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
