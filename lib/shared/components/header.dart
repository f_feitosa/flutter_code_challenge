import 'package:flutter/material.dart';

SizedBox Header({required String text}) {
  return SizedBox(
      height: 200.0,
      width: double.infinity,
      child: DecoratedBox(
        decoration: const BoxDecoration(
            color: Colors.blueGrey,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40.0),
                bottomRight: Radius.circular(40.0))),
        child: Center(
          child: Text(text,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.w900)),
        ),
      ));
}
