import 'package:flutter/material.dart';

OutlinedButton CustomOutlinedButton(
        {required String text,
        required VoidCallback callback,
        Color? textColor}) =>
    OutlinedButton(
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        side: const BorderSide(width: 2, color: Colors.deepOrangeAccent),
      ),
      onPressed: callback,
      child: Text(
        text,
        style: TextStyle(color: textColor ?? Colors.white),
      ),
    );
