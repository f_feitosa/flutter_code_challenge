enum CalculatorOperation { addition, subtraction, multiply, divide, squareRoot }

extension CalculatorOperationExt on CalculatorOperation {
  String get prettyName {
    switch (this) {
      case CalculatorOperation.addition:
        {
          return "Addition";
        }
      case CalculatorOperation.subtraction:
        {
          return "Subtraction";
        }
      case CalculatorOperation.multiply:
        {
          return "Multiply";
        }
      case CalculatorOperation.divide:
        {
          return "Divide";
        }
      case CalculatorOperation.squareRoot:
        {
          return "Square Root";
        }
    }
  }

  String get operationValue {
    switch (this) {
      case CalculatorOperation.addition:
        {
          return "+";
        }
      case CalculatorOperation.subtraction:
        {
          return "-";
        }
      case CalculatorOperation.multiply:
        {
          return "x";
        }
      case CalculatorOperation.divide:
        {
          return "/";
        }
      case CalculatorOperation.squareRoot:
        {
          return "√";
        }
    }
  }
}
