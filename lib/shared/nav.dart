import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/calculator/view/calculator_screen.dart';
import 'package:flutter_code_challenge/calculator/result/result_screen.dart';
import '../main.dart';
import 'calculator_operation.dart';

class Nav {
  static restart(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (_) => const MyApp()),
        (Route<dynamic> route) => false);
  }

  static toCalculator(
      {required BuildContext context,
      String? firstValue,
      required CalculatorOperation operation,
      String? previousNumber}) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CalculatorScreen(
                operation: operation, previousNumber: previousNumber)));
  }

  static toResult(
      {required BuildContext context, required String calculationResult}) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (_) => ResultScreen(calculationResult: calculationResult)),
        (Route<dynamic> route) => false);
  }
}
