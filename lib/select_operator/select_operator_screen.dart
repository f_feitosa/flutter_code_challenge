import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'package:flutter_code_challenge/shared/components/custom_outlined_button.dart';
import 'package:flutter_code_challenge/shared/components/header.dart';
import 'package:flutter_code_challenge/shared/nav.dart';

class SelectOperatorScreen extends StatelessWidget {
  const SelectOperatorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Header(text: "Welcome to Calculify"),
            const SizedBox(
              height: 20.0,
            ),
            const Text("Select the operation to start:",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w300)),
            const OperationButtons(),
          ],
        ),
      ),
    );
  }
}

class OperationButtons extends StatelessWidget {
  const OperationButtons({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.builder(
            itemCount: CalculatorOperation.values.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            itemBuilder: (_, int index) {
              return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                      width: double.infinity,
                      child: CustomOutlinedButton(
                          text: CalculatorOperation.values[index].prettyName,
                          callback: () {
                            Nav.toCalculator(
                                context: context,
                                operation: CalculatorOperation.values[index]);
                          })));
            }),
      ),
    );
  }
}
