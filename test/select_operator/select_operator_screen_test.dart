import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/select_operator/select_operator_screen.dart';
import 'package:flutter_test/flutter_test.dart';

import '../testable_widget.dart';

void main() {
  testWidgets('operatorScreen has a [header and message]',
      (WidgetTester tester) async {
    await tester.pumpWidget(BuildTestableWidget(const SelectOperatorScreen()));

    final headerFinder = find.text('Welcome to Calculify');
    final messageFinder = find.text('Select the operation to start:');

    expect(headerFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });

  testWidgets('operatorScreen has [five operation buttons]',
      (WidgetTester tester) async {
    await tester.pumpWidget(BuildTestableWidget(const SelectOperatorScreen()));

    final allButtons = find.byType(OutlinedButton);
    final additionButton = find.widgetWithText(OutlinedButton, "Addition");
    final subtractionButton =
        find.widgetWithText(OutlinedButton, "Subtraction");
    final multiplyButton = find.widgetWithText(OutlinedButton, "Multiply");
    final divideButton = find.widgetWithText(OutlinedButton, "Divide");
    final squareRootButton = find.widgetWithText(OutlinedButton, "Square Root");

    expect(allButtons, findsNWidgets(5));
    expect(additionButton, findsOneWidget);
    expect(subtractionButton, findsOneWidget);
    expect(multiplyButton, findsOneWidget);
    expect(divideButton, findsOneWidget);
    expect(squareRootButton, findsOneWidget);
  });
}
