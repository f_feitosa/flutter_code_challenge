

import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/calculator/view/calculator_screen.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../testable_widget.dart';

void main() {
  testWidgets('Should have Cancel button', (WidgetTester tester) async {
    await tester.pumpWidget(BuildTestableWidget(const CalculatorScreen(
      operation: CalculatorOperation.addition,
    )));

    final cancelButtonFinder = find.widgetWithText(OutlinedButton, "Cancel");

    expect(cancelButtonFinder, findsOneWidget);
  });

  group("Should have correct button for each operation", () {
    testWidgets('have insertNextValue button when not SquareRoot',
            (WidgetTester tester) async {
          await tester.pumpWidget(BuildTestableWidget(const CalculatorScreen(
            operation: CalculatorOperation.addition,
          )));

          final nextButtonFinder =
          find.widgetWithText(OutlinedButton, "Insert next value");

          expect(nextButtonFinder, findsOneWidget);
        });

    testWidgets('have Calculate button when SquareRoot',
            (WidgetTester tester) async {
          await tester.pumpWidget(BuildTestableWidget(const CalculatorScreen(
            operation: CalculatorOperation.squareRoot,
          )));

          final calculateButtonFinder =
          find.widgetWithText(OutlinedButton, "Calculate");

          expect(calculateButtonFinder, findsOneWidget);
        });
  });

  testWidgets('Should show Snackbar when calculating without value',
          (WidgetTester tester) async {
        await tester.pumpWidget(BuildTestableWidget(const CalculatorScreen(
          operation: CalculatorOperation.squareRoot,
        )));

        final calculateButtonFinder =
        find.widgetWithText(OutlinedButton, "Calculate");

        await tester.tap(calculateButtonFinder);
        expect(calculateButtonFinder, findsOneWidget);
        expect(find.text("You need to add a number."), findsNothing);
        await tester.pump(); // schedule animation
        expect(find.text("You need to add a number."), findsOneWidget);
      });

  testWidgets('Should update EditText when tapping on custom Calculator',
          (WidgetTester tester) async {
        await tester.pumpWidget(BuildTestableWidget(const CalculatorScreen(
          operation: CalculatorOperation.squareRoot,
        )));

        final buttonSeven = find.widgetWithText(ElevatedButton, "7");

        await tester.tap(buttonSeven);
        await tester.pump();

        expect(
          tester.widget(find.byType(TextField)),
          isA<TextField>()
              .having((t) => t.controller!.text, 'controller.text', "7"),
        );

        await tester.tap(buttonSeven);
        await tester.tap(buttonSeven);
        await tester.pump();

        expect(
          tester.widget(find.byType(TextField)),
          isA<TextField>()
              .having((t) => t.controller!.text, 'controller text', "777"),
        );
      });
}