import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_code_challenge/calculator/bloc/calculator_bloc.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  blocTest<CalculatorBloc, CalculatorState>(
    'emit correct [typing states] as user types',
    build: () => CalculatorBloc(operation: CalculatorOperation.addition),
    act: (bloc) => {
      bloc.add(const Typing(digit: "1")),
      bloc.add(const Typing(digit: "3")),
      bloc.add(const Typing(digit: ".")),
      bloc.add(const Typing(digit: "9"))
    },
    expect: () => <CalculatorState>[
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "1"),
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "13"),
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "13."),
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "13.9")
    ],
  );

  blocTest<CalculatorBloc, CalculatorState>(
    'emit correct [previousNumber] when requesting next screen',
    build: () => CalculatorBloc(operation: CalculatorOperation.addition),
    act: (bloc) => {
      bloc.add(const Typing(digit: "4")),
      bloc.add(const Typing(digit: "2")),
      bloc.add(InsertNextNumber()),
    },
    expect: () => <CalculatorState>[
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "4"),
      CalculatorView(
          operation: CalculatorOperation.addition, currentNumber: "42"),
      const InputNextNumber(
          operation: CalculatorOperation.addition, previousNumber: '42'),
    ],
  );

  group("emit correct [CalculationResult]", () {
    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [calculatorResult] after finishing calculating flow with one number',
      build: () => CalculatorBloc(operation: CalculatorOperation.squareRoot),
      act: (bloc) => {
        bloc.add(const Typing(digit: "1")),
        bloc.add(const Typing(digit: "6")),
        bloc.add(Calculate()),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.squareRoot, currentNumber: "1"),
        CalculatorView(
            operation: CalculatorOperation.squareRoot, currentNumber: "16"),
        CalculationResult(result: '4.0'),
      ],
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [calculatorResult] after finishing calculating with two numbers',
      build: () => CalculatorBloc(
          operation: CalculatorOperation.addition, previousNumber: "44"),
      act: (bloc) => {
        bloc.add(const Typing(digit: "1")),
        bloc.add(const Typing(digit: "6")),
        bloc.add(Calculate()),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.addition,
            previousNumber: "44",
            currentNumber: "1"),
        CalculatorView(
            operation: CalculatorOperation.addition,
            previousNumber: "44",
            currentNumber: "16"),
        CalculationResult(result: '60.0'),
      ],
    );
  });

  group("emit [errors]", () {
    // REQUESTED IN THE CODE CHALLENGE
    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [error] when trying to calculate squareRoot of negative number',
      build: () => CalculatorBloc(operation: CalculatorOperation.squareRoot),
      act: (bloc) => {
        bloc.add(const Typing(digit: "-")),
        bloc.add(const Typing(digit: "6")),
        bloc.add(Calculate()),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.squareRoot, currentNumber: "-"),
        CalculatorView(
            operation: CalculatorOperation.squareRoot, currentNumber: "-6"),
        CalculatorView(
            operation: CalculatorOperation.squareRoot,
            currentNumber: "-6",
            error:
            "You can't do this operation with negative numbers, please restart your calculation"),
        CalculatorView(
            operation: CalculatorOperation.squareRoot,
            currentNumber: "-6",
            error:
            null),
      ],
    );

    // REQUESTED IN THE CODE CHALLENGE
    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [error] when trying to calculate a division by 0',
      build: () => CalculatorBloc(
          operation: CalculatorOperation.divide, previousNumber: "13"),
      act: (bloc) => {
        bloc.add(const Typing(digit: "0")),
        bloc.add(Calculate()),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.divide,
            previousNumber: "13",
            currentNumber: "0"),
        CalculatorView(
            operation: CalculatorOperation.divide,
            previousNumber: "13",
            currentNumber: "0",
            error: "You can't divide by zero, please restart your calculation"),
        CalculatorView(
            operation: CalculatorOperation.divide,
            previousNumber: "13",
            currentNumber: "0",
            error: null),
      ],
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [error] when trying to calculate with no number',
      build: () => CalculatorBloc(operation: CalculatorOperation.squareRoot),
      act: (bloc) => {
        bloc.add(Calculate()),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.squareRoot,
            error: "You need to add a number."),
        CalculatorView(
            operation: CalculatorOperation.squareRoot,
            error: null),
      ],
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'emit correct [error] when trying to add decimal with no number',
      build: () => CalculatorBloc(operation: CalculatorOperation.addition),
      act: (bloc) => {
        bloc.add(const Typing(digit: ".")),
      },
      expect: () => <CalculatorState>[
        CalculatorView(
            operation: CalculatorOperation.addition,
            error:
            "You need to add at least one number before adding a decimal."),
        CalculatorView(
            operation: CalculatorOperation.addition,
            error: null),
      ],
    );
  });
}