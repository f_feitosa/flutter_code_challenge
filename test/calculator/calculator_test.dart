import 'package:flutter_code_challenge/calculator/bloc/calculator_bloc.dart';
import 'package:flutter_code_challenge/calculator/calculator.dart';
import 'package:flutter_code_challenge/calculator/calculator_exception.dart';
import 'package:flutter_code_challenge/shared/calculator_operation.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {

  group("should get correct operation result", (){
    test("addition", () {
      final calculatorView = CalculatorView(operation: CalculatorOperation.addition, currentNumber: "12", previousNumber: "30");

      final calculation = Calculator.calculate(calculatorView);

      expect(calculation, "42.0");
    });

    test("subtraction", () {
      final calculatorView = CalculatorView(operation: CalculatorOperation.subtraction, currentNumber: "12", previousNumber: "30");

      final calculation = Calculator.calculate(calculatorView);

      expect(calculation, "18.0");
    });

    test("divide", () {
      final calculatorView = CalculatorView(operation: CalculatorOperation.divide, currentNumber: "10", previousNumber: "120");

      final calculation = Calculator.calculate(calculatorView);

      expect(calculation, "12.0");
    });

    test("multiply", () {
      final calculatorView = CalculatorView(operation: CalculatorOperation.multiply, currentNumber: "7", previousNumber: "3");

      final calculation = Calculator.calculate(calculatorView);

      expect(calculation, "21.0");
    });

    test("square root", () {
      final calculatorView = CalculatorView(operation: CalculatorOperation.squareRoot, currentNumber: "4");

      final calculation = Calculator.calculate(calculatorView);

      expect(calculation, "2.0");
    });
  });

  group("should throw correct Exceptions based on user input", () {
    test('throw NeedsAtLeastOneNumberException when calculating with no numbers', () {

      final calculatorView = CalculatorView(operation: CalculatorOperation.addition);

      expect(() => Calculator.calculate(calculatorView), throwsA(isA<NeedsAtLeastOneNumberException>()));

    });

    test('throw CantSquareRootNegativeNumberException when square root a negative number', () {

      final calculatorView = CalculatorView(operation: CalculatorOperation.squareRoot, currentNumber: "-1");

      expect(() => Calculator.calculate(calculatorView), throwsA(isA<CantSquareRootNegativeNumberException>()));

    });

    test('throw CantDivideByZeroException when square root a negative number', () {

      final calculatorViewPreviousNevative = CalculatorView(operation: CalculatorOperation.divide, currentNumber: "0", previousNumber: "123");

      expect(() => Calculator.calculate(calculatorViewPreviousNevative), throwsA(isA<CantDivideByZeroException>()));

      final calculatorViewCurrentNevative = CalculatorView(operation: CalculatorOperation.divide, currentNumber: "123", previousNumber: "0");

      expect(() => Calculator.calculate(calculatorViewCurrentNevative), throwsA(isA<CantDivideByZeroException>()));
    });

    test('throw OperationNeedsTwoNumbers when there is only one number and opeartion is not square root', () {

      final calculatorView = CalculatorView(operation: CalculatorOperation.subtraction, currentNumber: "10");

      expect(() => Calculator.calculate(calculatorView), throwsA(isA<OperationNeedsTwoNumbers>()));
    });
  });
}