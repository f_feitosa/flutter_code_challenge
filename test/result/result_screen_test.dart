import 'package:flutter/material.dart';
import 'package:flutter_code_challenge/calculator/result/result_screen.dart';
import 'package:flutter_test/flutter_test.dart';

import '../testable_widget.dart';

void main() {
  testWidgets('resultScreen has [header], [message] and [right result]',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        BuildTestableWidget(const ResultScreen(calculationResult: "42.0")));

    final header = find.text('Congratulations ✨');
    final message = find.text('your result is:');
    final result = find.text('42.0');

    expect(header, findsOneWidget);
    expect(message, findsOneWidget);
    expect(result, findsOneWidget);
  });

  testWidgets('resultScreen has [Restart button]', (WidgetTester tester) async {
    await tester.pumpWidget(
        BuildTestableWidget(const ResultScreen(calculationResult: "42.0")));

    final resetButton = find.widgetWithText(OutlinedButton, "Restart");

    expect(resetButton, findsOneWidget);
  });
}
